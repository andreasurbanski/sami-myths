var viewHistory = [];

goranus.debug = true;

$("[view-name='artView']").imagesLoaded(function(){

   $(".masonry").masonry({
        itemSelector: ".tile",
    });

});

$("[view-name='creatureView']").imagesLoaded(function(){

   $(".masonry").masonry({
        itemSelector: ".tile",
    });

});

$("[view-name='discoverView']").imagesLoaded(function(){

   $(".masonry").masonry({
        itemSelector: ".tile",
    });

});

$("div.tile").on("click", function(){
	if ($(this).attr("href"))
		window.open($(this).attr("href"), "_blank");
});

$(function() {
    FastClick.attach(document.body);
});

$(".panzoom-image").panzoom({
	contain: 'invert',
	minScale: 1,
	increment: 0.8,
});

goranus.on("resizeWindow", function(event){
    $(window).trigger("resize");
});

goranus.on("BlockEvent::ViewChange", function(event){

    $(".masonry").masonry({
        transitionDuration: 0
    });

    if (goranus.settings.transition == "fade") {
        
        goranus.configure({
            transition: "immediate",
            updateViews: true,
            updateBlocks: true
        });

    }

    var name = event.value;
    
    if (viewHistory[viewHistory.length - 2] !== name) {
        
        setTimeout(function(){
            $.scrollTo(0, 300, {easing: 'easeOutCubic'});
        },
        1);

        viewHistory.push(name);
    }
    else {
        viewHistory.pop();
    }
    
    $('.close-navigation').click();
});

//document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {

    // Mock device.platform property if not available
    if (!window.device) {
        window.device = { platform: 'Browser' };
    }

    handleExternalURLs();
}

function handleExternalURLs() {
    // Handle click events for all external URLs
    if (device.platform.toUpperCase() === 'ANDROID') {
        $(document).on('click', 'a[href^="http"]', function (e) {
            var url = $(this).attr('href');
            navigator.app.loadUrl(url, { openExternal: true });
            e.preventDefault();
        });
    }
    else if (device.platform.toUpperCase() === 'IOS') {
        $(document).on('click', 'a[href^="http"]', function (e) {
            var url = $(this).attr('href');
            window.open(url, '_system');
            e.preventDefault();
        });
    }
    else {
        // Leave standard behaviour
    }
}


// App init & fastclick

window.addEventListener("load", function(){

    FastClick.attach(document.getElementsByTagName("body")[0]);

    documentReady();

    document.addEventListener("deviceready", function(){
        onDeviceReady();
    })

});


