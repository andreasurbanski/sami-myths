/* BlockJS Version 0.2.1, 28.4.2015
 *
 * TODO (1) BlockGroup::Change trigger (2) View binds (3) hash change double call?
 * (4) extend router actions (8) Group update
 *
 * CHANGES IN THIS VERSION:
 *
 * View full changelog at https://github.com/andreasur/blockjs/blob/master/CHANGELOG
 *
 * DESCRIPTION:
 *
 * BlockJS is a lightweight yet powerful MVC framework based on JavaScript's
 * custom events. It aims to completely separate UI logic from the application's data model
 * thus making scalable applications more maintainable and easier to develop.
 *
 * COPYRIGHT (C) 2015 Andreas Urbanski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function BlockApp(root)
{
	this.debug = true;

	/* BlockJS custom event format. The default event name is formed like this:
	block name + event delimiter + event name */
	this.eventDelimiter = "::";

	/* BlockJS regocnized DOM attributes. Supported attributes are automatically parsed
	into object properties */
	this.attributeNames = {

		/* An element that has the 'app-name' attribute is initialized into BlockApp object */
		appName: "app-name",

		/* App settings such as transition, transitionTime etc. are delimited via ; and are
		by default applied to all views and blocks */
		appSettings: "app-settings",

		/* Used to determine which view is deployed first */
		appInit: "app-init",

		/* Elements that have the 'view-name' attribute are initialized into block views */
		viewName: "view-name",

		/* Elements that have the 'block-name' attribute are initialized into blocks, which are
		the fundamental primitives for BlockJS applications */ 
		blockName: "block-name",

		/* When blocks are in groups, they are indexed. This attribute preserved the original
		block name and is not set by user */
		blockUnindexedName: "block-name-unindexed",

		/* The block init attribute determines whether a block is shown or hidden initially
		and can also be used to declare the view that the block accompanies. Example values are
		hide, show or <view name> */
		blockInit: "block-init",

		/* Similar to app settings. Allows you to define individual options for the block */
		blockSettings: "block-settings",

		/* The block index in dynamic groups */
		blockIndex: "block-index",

		/* Additional value that overrides the default block value can be specified via this
		attribute */
		blockValue: "block-value",

		/* Reserved */
		blockHide: "block-hide",

		/* Reserved */
		blockDeploy: "block-deploy",

		/* Elements that have the 'block-group' attribute are initialized into a block groups, which
		are dynamic collections of blocks */
		blockGroupName: "block-group",

		/* Block groups can be nested which leads to indexed block groups */
		blockGroupIndex: "block-group-index",

		/* This is the block group index relative to the first parent of the block group */
		blockGroupRelativeIndex: "block-group-relative-index",

		/* Trigger attributes take in multiple event names delimited via space. The action part
		determines when the event is fired. By default the current value of the block is used
		with the event, but you can set alternative values by placing them in parenthesis after
		the event name:

		eventName + delimiter + (value to use)
		*/

		/* The 'block-change' attribute is used to define which events are fired when the
		element changes */
		triggerChange: "block-change",

		/* The 'block-click' attribute is used to define which events are fired when the
		element is clicked */
		triggerClick: "block-click",

		/* The 'block-double-click' attribute is used to define which events are fired when the
		element is double clicked */
		triggerDoubleClick: "block-double-click",

		/* The 'block-hover' attribute is used to define which events are fired when the
		element is hovered with mouse */
		triggerHover: "block-hover",

		/* The 'block-hover-out' attribute is used to define which events are fired when the
		hovering stops */
		triggerHoverOut: "block-hover-out",

		/* The 'block-write' attribute is used to define which events are fired when the user
		writes to the element */
		triggerKeydown: "block-write",

		/* The 'block-drag' attribute is used to define which events are fired when the
		element is dragged */
		triggerDrag: "block-drag",
	};

	/* BlockJS recognized default events. All blocks and views listen to these events
	by default. */
	this.eventNames = {
		/* Deploy event is fired when a view is deployed */
		deploy: this.eventDelimiter + "Deploy",

		/* Withdraw event is fired when a view is withdrawn */
		withdraw: this.eventDelimiter + "Withdraw",

		/* By firing a 'Load' event you can load values onto blocks */
		load: this.eventDelimiter + "Load",

		/* 'Change' event is fired whenever a block's value changes or it's
		interacted with */
		change: this.eventDelimiter + "Change",

		/* By firing a 'Show' event you can display hidden views or blocks */
		show: this.eventDelimiter + "Show",

		/* By firing a 'Hide' event you can hide visible views or blocks */
		hide: this.eventDelimiter + "Hide",

		/* By firing a 'AddClass' event you can add a class or multiple classes to views or blocks */
		addClass: this.eventDelimiter + "AddClass",

		/* By firing a 'RemoveClass' event you can remove a class or multiple classes from views 
		or blocks */
		removeClass: this.eventDelimiter + "RemoveClass",

		/* Set custom attributes to views or blocks */
		setAttributes: this.eventDelimiter + "SetAttributes",

		/* Remove custom attributes from views or blocks */
		removeAttributes: this.eventDelimiter + "RemoveAttributes",

		/* Generic events are fired by BlockJS by default */

		/* 'ViewChange' event is fired whenever app view is changed */
		genericViewChange: "BlockEvent" + this.eventDelimiter + "ViewChange",
	};

	/* These default Javascript events correspond to the following BlockJS event
	names */
	this.eventNativePairs = {
		change: "triggerChange",
		click: "triggerClick",
		dblclick: "triggerDoubleClick",
		mouseover: "triggerHover",
		keydown: "triggerKeydown",
		drag: "triggerDrag"
	};

	/* BlockJS recognized settings */
	this.settingsNames = {
		/* Determines which function to use for view or block transitions. Possible values:
		immediate, fade */
		transition: "transition",

		/* Time in milliseconds for animated transitions */
		transitionTime: "transition-time"
	}

	/* Reference to generic functions in BlockApp */
	this.functions = new BlockApp.Functions(this);

	/* BlockJS default data controller. Can be used to access block values because it syncs
	automatically by listening to block change events */
	this.data = new BlockApp.Data(this);
 
	/* Name of the application and reference to apps DOM root node */
	this.name = root.getAttribute(this.attributeNames.appName);
	this.root = root;

	/* Default settings for the app */
	this.settings = {};
	this.settings.transition = "immediate";
	this.settings.transitionTime = 250;

	/* List of binds in the app */
	this.binds = {};

	/* BlockJS views */
	this.views = {};

	/* BLockJS block groups */
	this.blockGroups = {};

	/* BlockJS blocks */
	this.blocks = {};

	this.init();
}

/**
 * notice() Log debug notice message
 * @param {String} message
 */
BlockApp.prototype.notice = function(message)
{
	if (this.debug)
	console.log("BlockJS notice: " + message);
}

/**
 * warning() Log debug warning message
 * @param {String} message
 */
BlockApp.prototype.warning = function(message)
{
	if (this.debug)
	console.log("BlockJS warning: " + message);
}

/**
 * error() Debug error message (usually fatal)
 * @param {String} message
 */
BlockApp.prototype.error = function(message)
{
	if (this.debug)
	console.log("BlockJS error: " + message);
}

/**
 * init() Initialize application settings, views, block groups, blocks, view router.
 * Then parse app-init attribute and deploy to initial view. Finally update the default
 * data controller
 */
BlockApp.prototype.init = function()
{
	this.initSettings();

	this.initViews();

	this.initBlockGroups();

	this.initBlocks();

	this.initRouter();

	// Init to view
	var appInit = this.root.getAttribute(this.attributeNames.appInit);
	if (appInit) this.deployView(appInit);

	// Update data when all initializations are complete
	this.data.update(true);
}

/**
 * initViews() Initialize views inside the application recursively
 * @param {Object HTMLElement} root
 * @param {Number} level
 * @param {Boolean} inView
 * @param {String} chain
 */
BlockApp.prototype.initViews = function(root, level, inView, chain)
{
	var level = (typeof level !== "undefined") ? level : 0;
	var children = (typeof root !== "undefined") ? root.children : this.root.children;
	var inView = (typeof inView !== "undefined") ? inView : false;

	/* View chain is used to collect information about the view hierarchy. Subviews
	(views inside views) need to know all of their parents because when they are deployed
	the parents need to be deployed as well */
	var chain = (typeof chain !== "undefined") ? chain : "";

	/* Increase level only when in view node (we are not interested in wrapper nodes around
	views) */
	if (inView) level++;

	for (var child, i=0; child = children[i]; i++) {

		/* Initialize next if child is recognized as BlockJS view */
		if (child.hasAttribute(this.attributeNames.viewName)) {
			
			var name = child.getAttribute(this.attributeNames.viewName);

			this.initView(child, level, chain);

			/* Append to chain and call self */
			this.initViews(child, level, true, chain + name + ">");
		}

		/* Other types of DOM nodes. Keep traversing */
		else {
			this.initViews(child, level, false, chain);
		}

	}
}

/**
 * initBlockGroups() Initialize 1st level block groups inside the application. Block groups within block groups
 * must be initialized dynamically when the root group value is set
 */
BlockApp.prototype.initBlockGroups = function()
{
	var nodeList = this.root.querySelectorAll("[" + this.attributeNames.blockGroupName + "]");
	var blockGroups = [];

	// Convert nodeList to blockGroups array
	for(var i = nodeList.length; i--; blockGroups.unshift(nodeList[i]));

	for (var blockGroup, i=0; blockGroup = blockGroups[i]; i++) {

		// Find sub groups inside the current block group
		var subGroups = blockGroup.querySelectorAll("[" + this.attributeNames.blockGroupName + "]");

		if (subGroups !== null) {
			for (var subGroup, j=0; subGroup = subGroups[j]; j++) {
				var index = blockGroups.indexOf(subGroup);

				// Don't init sub groups
				if (index > -1)
					blockGroups.splice(index, 1);
			}
		}

		this.initBlockGroup(blockGroup);
	}
}

/**
 * initBlocks() Initialize all blocks found inside the application. This does not include blocks found in
 * block groups (template) because groups are initialized first, their content saved as a template
 * and then destroyed
 */
BlockApp.prototype.initBlocks = function()
{
	var blocks = this.root.querySelectorAll("[" + this.attributeNames.blockName + "]");

	for (var block, i=0; block = blocks[i]; i++) {
		this.initBlock(block);
	}
}

/**
 * initSettings() Parse application settings. The valid format for settings is:
 * setting name + : + setting value + ;
 */
BlockApp.prototype.initSettings = function()
{
	/* Find settings string from application root's attribute */
	var settings = this.root.getAttribute(this.attributeNames.appSettings);
	if (settings == null) return;

	/* Split by delimiter */
	settings = settings.split(";");

	/* Parse each setting */
	for (var setting, i=0; setting = settings[i]; i++) {
		
		/* Trim whitespace */
		setting = setting.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

		var keyValue = setting.split(":");

		if (keyValue.length > 1){
			
			/* Trim whitespace */
			var key = keyValue[0].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var value = keyValue[1].replace(/^\s\s*/, '').replace(/\s\s*$/, '');

			try {
			switch(key)
			{
			case this.settingsNames.transition:
				this.settings.transition = value;
				break;

			case this.settingsNames.transitionTime:
				this.settings.transitionTime = parseInt(value);
				break;

			default:
				throw "unrecognized";
				break;

			// Extend app settings here
			}
			}
			// Error: invalid setting
			catch(e) {
				this.warning("unrecognized setting '" + setting + "'");
			}

		}
	}
}

/**
 * initRouter() Initialize view router. BlockJS uses the conventional hash notation
 * for views, which changes every time a view is deployed or can be used to deploy
 * views by setting it manually
 */
BlockApp.prototype.initRouter = function()
{
	var _this = this;

	window.addEventListener("hashchange", function(event){
		
		if (event.skipDeploy)
			return;

		var newView = window.location.hash.substring(1);
		_this.deployView(newView);
	});
}

/**
 * initView() Initialize a view object. In BlockJS views are individual sections
 * in the application that can be deployed to be visible. Only one same level view
 * can be visible at any given time. Subviews can be visible while the parent view
 * is active.
 * @param {Object HTMLElement} view
 * @param {Number} level
 * @param {String} chain
 */
BlockApp.prototype.initView = function(view, level, chain)
{
	var _this = this;

	/* Find the name from the DOM attribute */
	var name = view.getAttribute(this.attributeNames.viewName);

	/* Save all object properties in custom the custom 'bjs' DOM attribute */
	view.bjs = new Object();
	view.bjs.name = name;
	view.bjs.level = (typeof level !== "undefined") ? level : 0;

	/* All parent views of the view that need to be deployed in order for the view to
	be visible when deployed */
	view.bjs.chain = (typeof chain !== "undefined") ? chain.split(">").filter(Boolean) : [];

	/* Store the default opacity and position values so they can be restored when hidden
	view is deployed again */
	view.bjs.defaultOpacity = window.getComputedStyle(view, null).getPropertyValue("opacity");
	view.bjs.defaultPosition = window.getComputedStyle(view, null).getPropertyValue("position");
	view.bjs.defaultLeft = window.getComputedStyle(view, null).getPropertyValue("left");

	/* Get transition settings from app parent initiallly */
	view.bjs.transition = this.settings.transition;
	view.bjs.transitionTime = this.settings.transitionTime;

	view.bjs.additionalTriggers = {}
	view.bjs.additionalTriggers.deploy = [];
	view.bjs.additionalTriggers.withdraw = [];

	view.bjs.active = false;

	/* The bind method binds a custom event to the view node */
	view.bjs.bind = function(eventName, callback){
		_this.functions.bind(view, eventName, callback);
	};

	/* The deploy method makes the view visible */
	view.bjs.deploy = function(){
		view.bjs.active = true;
		_this.functions.show(view);
		//_this.fire(name + _this.eventNames.deploy, name);
	};

	/* The withdraw method hides the view */
	view.bjs.withdraw = function(){
		view.bjs.active = false;
		_this.functions.hide(view);
		//_this.fire(name + _this.eventNames.withdraw, name);
	};

	view.bjs.blockjsType = "view";

	/* Store in app parent */
	this.views[view.bjs.name] = view;

	/* Setup default binds */
	this.setupViewBinds(view);

	/* Hide the view initially */
	view.bjs.withdraw();
}

/**
 * initBlock() Initialize a block object. Blocks are the application's basic primitives.
 * Their value, class, attributes etc. can be changed and their status monitored. Essentially
 * blocks are just HTML elements that are bound to and trigger BlockJS' default events
 * @param {Object HTMLElement} block
 * @param {String} group
 */
BlockApp.prototype.initBlock = function(block, group)
{
	var _this = this;

	/* Find the name from the DOM attribute */
	var name = block.getAttribute(this.attributeNames.blockName);

	block.bjs = new Object();

	/* When block groups contain blocks the reference to the group is stored because
	it's later needed in order to tell which blocks belong to which groups */
	if (typeof group !== "undefined")
	block.bjs.group = group;

	block.bjs.name = name;

	/* Find the type of the block based on the HTML element tag name */
	block.bjs.type = this.findBlockType(block);

	/* Value override from DOM attribute */
	block.bjs.value = block.getAttribute(this.attributeNames.blockValue);

	/* Index from DOM attribute */
	block.bjs.index = block.getAttribute(this.attributeNames.blockIndex);

	/* Default index -1 (unindexed) */
	if (typeof block.bjs.index === null)
	block.bjs.index = -1;
	
	/* Find the unindexed name when dealing with blocks that are within block groups */
	block.bjs.unindexedName = block.getAttribute(this.attributeNames.blockUnindexedName);

	if (typeof block.bjs.unindexedName === null)
	block.bjs.unindexedName = name;

	block.bjs.blockjsType = "block";

	/* Store the default opacity and position values so they can be restored when hidden
	view is deployed again */
	block.bjs.defaultOpacity = window.getComputedStyle(block, null).getPropertyValue("opacity");
	block.bjs.defaultPosition = window.getComputedStyle(block, null).getPropertyValue("position");
	block.bjs.defaultLeft = window.getComputedStyle(block, null).getPropertyValue("left");

	/* By default the block will listen to this event then trigger the defaultTrigger */
	block.bjs.defaultListener = this.findBlockListener(block.bjs.type);
	block.bjs.defaultTrigger = name + this.eventNames.change;

	/* list of event bindings on the block */
	block.bjs.binds = {};

	/* List of event triggers the block may fire */
	block.bjs.triggers = {};

	/* Settings from app parent */
	block.bjs.transition = this.settings.transition;
	block.bjs.transitionTime = this.settings.transitionTime;

	/* Get block value function (depends on block type) */
	block.bjs.get = this.findGetFunction(block);

	/* Set block value function (depends on block type) */
	block.bjs.set = this.findSetFunction(block);

	block.bjs.bind = function(eventName, callback){
		_this.functions.bind(block, eventName, callback);
	}

	block.bjs.show = function(){
		_this.functions.show(block);
	};

	block.bjs.hide = function(){
		_this.functions.hide(block);
	};

	block.bjs.addClass = function(classNames){
		_this.functions.addClass(block, classNames);
	};

	block.bjs.removeClass = function(classNames){
		_this.functions.removeClass(block, classNames);
	};

	block.bjs.setAttributes = function(attributes){
		_this.functions.setAttributes(block, attributes);
	}

	block.bjs.removeAttributes = function(attributes){
		_this.functions.removeAttributes(block, attributes);
	}
	
	/* When block value is loaded it is passed through inFilter */
	block.bjs.inFilter = function(value){
		return value;
	};

	/* When block value is checked it is passed through outFilter */
	block.bjs.outFilter = function(value){
		return value;
	};

	this.setupBlockSettings(block);

	if (typeof this.blocks[name] !== "undefined") {
	// Warning: Block name conflict
	this.warning("block name conflict '" + name + "'");
	}
 
	this.blocks[name] = block;

	// Parse block init
	this.setupBlockInit(block);

	// Setup block binds
	this.setupBlockBinds(block);

	// Setup block triggers and listeners
	this.setupBlockTriggers(block);
}

/**
 * initBLockGroup() Initialize a block group object. BLock groups are dynamic collections
 * of blocks. They behave much like blocks but they can be loaded with complex values like
 * arrays and objects and their contents change based on the amount of values loaded and their
 * initial template
 * @param {Object HTMLElement} blockGroup
 * @param {String} parentGroupName
 */
BlockApp.prototype.initBlockGroup = function(blockGroup, parentGroupName)
{
	var _this = this;
	var name = blockGroup.getAttribute(this.attributeNames.blockGroupName);

	blockGroup.bjs = new Object();

	blockGroup.bjs.name = name;
	blockGroup.bjs.unindexedName = null;
	blockGroup.bjs.template = blockGroup.innerHTML;
	blockGroup.bjs.group = (typeof parentGroupName !== "undefined") ? parentGroupName : null;
	blockGroup.bjs.index = blockGroup.getAttribute("block-group-index");

	if (blockGroup.bjs.index == null)
	blockGroup.bjs.index = -1;

	else
	blockGroup.bjs.unindexedName = blockGroup.bjs.name.replace(new RegExp(blockGroup.bjs.index + '$'), '');

	blockGroup.bjs.defaultOpacity = window.getComputedStyle(blockGroup, null).getPropertyValue("opacity");
	blockGroup.bjs.defaultPosition = window.getComputedStyle(blockGroup, null).getPropertyValue("position");
	blockGroup.bjs.defaultLeft = window.getComputedStyle(blockGroup, null).getPropertyValue("left");

	blockGroup.bjs.transition = this.settings.transition;
	blockGroup.bjs.transitionTime = this.settings.transitionTime;

	blockGroup.innerHTML = "";

	blockGroup.bjs.destroy = function(self){

		var self = (typeof self === "undefined") ? false : self;

		var blocks = blockGroup.querySelectorAll("[" + _this.attributeNames.blockName + "]");

		for (var block, i=0; block = blocks[i]; i++) {
			if (block.bjs.group == blockGroup.bjs.name) {

				_this.destroyBlock(block.bjs.name);
			}
		}

		var blockGroups = blockGroup.querySelectorAll("[" + _this.attributeNames.blockGroupName + "]");

		for (var subBlockGroup, i=0; subBlockGroup = blockGroups[i]; i++) {
			if (subBlockGroup.bjs.group == blockGroup.bjs.name) {

				subBlockGroup.bjs.destroy(true);
				subBlockGroup.innerHTML = "";
			}
		}

		if (self == true)
		_this.destroyBlockGroup(blockGroup.bjs.name);
	};

	blockGroup.bjs.inFilter = function(value){
		return value;
	}

	blockGroup.bjs.outFilter = function(value){
		return value;
	}

	blockGroup.bjs.populate = function(count){

		var temp = document.createElement("div");
		var wrap = "";

		for (var i=0; i<count; i++){

			temp.innerHTML = blockGroup.bjs.template;

			var blocks = temp.querySelectorAll("[" + _this.attributeNames.blockName + "]");

			for (var block, j=0; block = blocks[j]; j++){

				var name = block.getAttribute(_this.attributeNames.blockName);

				var index = "";
				if (blockGroup.bjs.index !== -1) index += blockGroup.bjs.index.toString();

				if (count > 1) {
					name += i;
					index += i;
				}

				block.setAttribute(_this.attributeNames.blockName, name);
				block.setAttribute(_this.attributeNames.blockIndex, i);

				block.setAttribute(_this.attributeNames.blockUnindexedName, name.replace(new RegExp(index + '$'), ''));
			}

			wrap += temp.innerHTML;

		}

		blockGroup.innerHTML = wrap;

		var nodeList = blockGroup.querySelectorAll("[" + _this.attributeNames.blockGroupName + "]");
		var subBlockGroups = [];

		// Convert nodeList to subBlockGroups array
		for(var j = nodeList.length; j--; subBlockGroups.unshift(nodeList[j]));

		for (var subBlockGroup, j=0; subBlockGroup = subBlockGroups[j]; j++) {

			// Find sub groups inside the current block group
			var subGroups = subBlockGroup.querySelectorAll("[" + _this.attributeNames.blockGroupName + "]");

			if (subGroups !== null) {
				for (var subGroup, n=0; subGroup = subGroups[n]; n++) {
					var index = subBlockGroups.indexOf(subGroup);

					// Don't init sub groups
					if (index > -1)
						subBlockGroups.splice(index, 1);
				}
			}

			var name = subBlockGroup.getAttribute(_this.attributeNames.blockGroupName);
			var index = ""

			if (blockGroup.bjs.index !== -1) index += blockGroup.bjs.index.toString();
			if (count > 1) index += j;

			name += index;

			subBlockGroup.setAttribute(_this.attributeNames.blockGroupName, name);
			subBlockGroup.setAttribute(_this.attributeNames.blockGroupIndex, index);
			subBlockGroup.setAttribute(_this.attributeNames.blockGroupRelativeIndex, j);

			_this.initBlockGroup(subBlockGroup, blockGroup.bjs.name);
		}

		var blocks = blockGroup.querySelectorAll("[" + _this.attributeNames.blockName + "]");

		for (var block, i=0; block = blocks[i]; i++) {
			_this.initBlock(block, blockGroup.bjs.name);
		}
	};

	blockGroup.bjs.set = function(multipleValues){

		blockGroup.bjs.destroy();

		if (Object.prototype.toString.call(multipleValues) === "[object Object]") {
			var multipleValues = [multipleValues];
		}

		if (Object.prototype.toString.call(multipleValues) === "[object Array]")
		{
			blockGroup.bjs.populate(multipleValues.length);

        	for (var item, i=0; item = multipleValues[i]; i++) {
			
				if (typeof item !== "object")
					continue;

				for (var name in item) {
					
					var value = item[name];
					var eventName = name;

					if (blockGroup.bjs.index !== -1) eventName += blockGroup.bjs.index;

					if (multipleValues.length > 1) eventName += i;

					_this.fire(eventName + _this.eventNames.load, value);
				}

			}
		}
	};

	blockGroup.bjs.get = function() {
		var value = [];
		var index = -1;

		var blockGroups = blockGroup.querySelectorAll("[" + _this.attributeNames.blockGroupName + "]");

		for (var subBlockGroup, j=0; subBlockGroup = blockGroups[j]; j++) {

			if (subBlockGroup.bjs.group == blockGroup.bjs.name) {

				var index = parseInt(subBlockGroup.getAttribute(_this.attributeNames.blockGroupRelativeIndex));

				if (typeof value[index] === "undefined")
					value[index] = {};

				if (subBlockGroup.bjs.unindexedName !== null)
					value[index][subBlockGroup.bjs.unindexedName] = subBlockGroup.bjs.get();
			}
		}

		var blocks = blockGroup.querySelectorAll("[" + _this.attributeNames.blockName + "]");

		for (var block, j=0; block = blocks[j]; j++) {

			if (block.bjs.group == blockGroup.bjs.name) {

				var index = parseInt(block.getAttribute(_this.attributeNames.blockIndex));

				if (typeof value[index] === "undefined")
					value[index] = {};

				value[index][block.bjs.unindexedName] = block.bjs.get();
			}
		}

		return blockGroup.bjs.outFilter(value);
	}

	blockGroup.bjs.blockjsType = "blockGroup";

	this.setupBlockGroupBinds(blockGroup);

	this.blockGroups[name] = blockGroup;
}

/**
 * setupViewBinds() Setup default listeners for an application view
 * @param {Object HTMLElement} view
 */
BlockApp.prototype.setupViewBinds = function(view)
{
	var _this = this;

	var name = view.bjs.name;

	/* Listen to 'Show' event to deploy view when triggered */
	this.createBinds(name + this.eventNames.show, function(event){
		_this.deployView(name);
	});

	/* Listen to 'Hide' event to withdraw view when triggered (note: you might lose all views) */
	this.createBinds(name + this.eventNames.hide, function(event){
		view.bjs.withdraw();

		/* Fire 'Withdraw' event */
		_this.fire(name + _this.eventNames.withdraw, name);
	});
}

/**
 * setupBlockGroupBinds() Setup default listeners for a block group
 * @param {Object HTMLElement} blockGroup
 */
BlockApp.prototype.setupBlockGroupBinds = function(blockGroup)
{
	var _this = this;
	var name = blockGroup.bjs.name;

	/* Listen to 'Load' event for loading complex value into the block group */
	this.createBinds(name + this.eventNames.load, function(event){
		if (typeof event.value === "undefined")
			return;

		blockGroup.bjs.set(blockGroup.bjs.inFilter(event.value));
	});
}

/**
 * setupBlockSettings() Parse block settings from DOM attribute
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.setupBlockSettings = function(block)
{
	var settings = block.getAttribute(this.attributeNames.blockSettings);
	if (settings == null) return;

	settings = settings.split(";");

	for (var setting, i=0; setting = settings[i]; i++) {

		setting = setting.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

		var keyValue = setting.split(":");

		if (keyValue.length > 1){
		
			var key = keyValue[0].replace(/^\s\s*/, '').replace(/\s\s*$/, '');;
			var value = keyValue[1].replace(/^\s\s*/, '').replace(/\s\s*$/, '');;

			switch(key)
			{
			case this.settingsNames.transition:
				block.bjs.transition = value;
				break;

			case this.settingsNames.transitionTime:
				block.bjs.transitionTime = parseInt(value);
				break;

			// Extend block settings here

			default:
				this.warning("Unrecognized block setting '" + setting + "'");
				break;
			}

		}
	}
}

/**
 * setupBlockInit() Parse block initial state and additional view triggers
 * from DOM attribute
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.setupBlockInit = function(block)
{
	var _this = this;

	var blockInit = block.getAttribute(this.attributeNames.blockInit);

	if (blockInit == null)
		return;

	blockInit = blockInit.split(" ");

	for (var initOption, i=0; initOption = blockInit[i]; i++)
	{
		switch(initOption)
		{
		case "hide":
			block.bjs.hide();
			break;
		case "show":
			block.bjs.show();
			break;
		// Extend block init options here
		}
		if (typeof _this.views[initOption] !== "undefined"){
			_this.views[initOption].bjs.additionalTriggers.deploy.push(block.bjs.name + _this.eventNames.show);
			_this.views[initOption].bjs.additionalTriggers.withdraw.push(block.bjs.name + _this.eventNames.hide);
		}
	}
}

/**
 * setupBlockBinds() Setup default listeners for a block
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.setupBlockBinds = function(block)
{
	var name = block.bjs.name;

	this.createBinds(name + this.eventNames.load, function(event){
		if (typeof event.value === "undefined")
			return;

		block.bjs.set(block.bjs.inFilter(event.value));
	});

	this.createBinds(name + this.eventNames.addClass, function(event){
		if (typeof event.value === "undefined")
			return;

		block.bjs.addClass(event.value);
	});

	this.createBinds(name + this.eventNames.removeClass, function(event){
		if (typeof event.value === "undefined")
			return;
		
		block.bjs.removeClass(event.value);
	});

	this.createBinds(name + this.eventNames.setAttributes, function(event){
		if (typeof event.value === "undefined")
			return;

		block.bjs.setAttributes(event.value);
	});

	this.createBinds(name + this.eventNames.removeAttributes, function(event){
		if (typeof event.value === "undefined")
			return;
		
		block.bjs.removeAttributes(event.value);
	});

	this.createBinds(name + this.eventNames.show, function(event){
		block.bjs.show();
	});

	this.createBinds(name + this.eventNames.hide, function(event){
		block.bjs.hide();
	});
}

/**
 * setupBlockTriggers() Setup default triggers for a block
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.setupBlockTriggers = function(block)
{
	var defaultTriggerName = block.bjs.name + this.eventNames.change;
	var defaultListener = block.bjs.defaultListener;

	for (var nativeEventName in this.eventNativePairs) {
		
		var customName = this.eventNativePairs[nativeEventName];
		var customAttribute = this.attributeNames[customName];

		var triggers = block.getAttribute(customAttribute);

		if (triggers !== null) {
			
			/* Regular expression magic explained:

			(?:         # non-capturing group
			  [^\s\(]+   # anything that's not a space or a opening parenthesis
			  |         #   or…
			  "         # opening parenthesis
			    [^"]*   # …followed by zero or more chacacters that are not a parenthesis
			  "         # …closing parenthesis
			)+          # each mach is one or more of the things described in the group
			
			*/
			triggers = triggers.match(/(?:[^\s\(]+|\([^\)]*\))+/g);

			block.bjs.triggers[nativeEventName] = triggers;

			if (defaultListener !== nativeEventName)
				this.createTriggers(block, nativeEventName, null, triggers);
		}
	}

	if (typeof block.bjs.triggers[defaultListener] !== "undefined")
		block.bjs.triggers[defaultListener].push(defaultTriggerName);

	else
		block.bjs.triggers[defaultListener] = [defaultTriggerName];

	this.createTriggers(block, defaultListener, null, block.bjs.triggers[defaultListener]);
}

/**
 * destroyBlockGroup() Destroy a block group completely removing
 * it from DOM, removing all associated binds and triggers and
 * deleting references from app root
 * @param {String} blockGroupName
 */
BlockApp.prototype.destroyBlockGroup = function(blockGroupName)
{
	if (typeof this.blockGroups[blockGroupName] === "undefined")
		return;

	var blockGroup = this.root.querySelector("[" + this.attributeNames.blockGroupName + "='" + blockGroupName + "']");

	if (blockGroup == null) {
		blockGroups = this.blockGroups[blockGroupName];
	}
	else {
		for (var blockGroupBind in blockGroup.bjs.binds) {
			this.removeTriggers(blockGroup, blockGroupBind);
		}
	}

	for (var bindName in this.binds){
		if (bindName.indexOf(blockGroupName + this.eventDelimiter) !== -1)
			this.removeBinds(bindName);
	}

	for (var blockGroupBind in this.blockGroups[blockGroupName].bjs.binds)
		this.removeTriggers(blockGroup, blockGroupBind);

	delete this.blockGroups[blockGroupName];
}

/**
 * destroyBlock() Destroy a block completely removing
 * it from DOM, removing all associated binds and triggers and
 * deleting references from app root
 * @param {String} blockName
 */
BlockApp.prototype.destroyBlock = function(blockName)
{
	if (typeof this.blocks[blockName] === "undefined"){
		return;
	}

	var block = this.root.querySelector("[" + this.attributeNames.blockName + "='" + blockName + "']");

	if (block == null) {
		block = this.blocks[blockName];
	}

	else {
		for (var blockBind in block.bjs.binds)
			this.removeTriggers(block, blockBind);
	}

	for (var bindName in this.binds) {
		if (bindName.indexOf(blockName + this.eventDelimiter) !== -1)
			this.removeBinds(bindName);
	}

	for (var blockBind in this.blocks[blockName].bjs.binds)
		this.removeTriggers(this.blocks[blockName], blockBind);

	delete this.blocks[blockName];
}

/**
 * findBlockType() Find the blockjsType of a block depending
 * on HTML element type.
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.findBlockType = function(block)
{
	var blockTag = block.nodeName.toLowerCase();

	if (["input", "textarea"].indexOf(blockTag) > -1)
		return "input";

	if (["button", "a"].indexOf(blockTag) > -1)
		return "control";

	if (["img"].indexOf(blockTag) > -1)
		return "media";

	if (block.classList.contains("slider"))
		return "slider";

	return "static";
}

/**
 * findBlockListener() Find the default listener for a block
 * @param {String} blockType
 */
BlockApp.prototype.findBlockListener = function(blockType)
{
	if (blockType == "control")
		return "click";

	else if (blockType == "slider")
		return "slide";

	return "change";
}

/**
 * findEventAttributes() Find the default attributes for block realted
 * BlockJS custom events
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.findEventAttributes = function(block)
{
	var value = block.bjs.outFilter(block.bjs.get());
	var name = block.bjs.name;
	var index = block.bjs.index;

	return {value: value, name: name, index: index};
}

/**
 * findGetFunction() Find the function that correctly returns a block's
 * value
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.findGetFunction = function(block)
{
	switch (block.bjs.type)
	{
	case "static": return function(){ 
			return block.innerHTML;
		}
	case "control": return function(){
			return block.bjs.value;
	}
	case "input": return function(){
			return block.value;
		}
	case "media": return function(){
			return block.src;
		}
	case "slider": return function(){
			return block.val();
		}
	default: return function(){
			return null;
		}
	}
}

/**
 * findSetFunction() Find the function that correctly sets a block's
 * value
 * @param {Object HTMLElement} block
 */
BlockApp.prototype.findSetFunction = function(block)
{
	switch (block.bjs.type)
	{
	case "static": return function(value){ 
			block.innerHTML = value;
		}
	case "control": return function(value){
			block.bjs.value = value;
	}
	case "input": return function(value){
			block.value = value;
		}
	case "media": return function(value){
			block.src = value;
		}
	case "slider": return function(value){
			block.val() = value;
		}
	default: return function(value){
			return;
		}
	}
}

/**
 * on() Add a custom event listener to app root
 * @param {String} eventName
 * @param {Function} callback
 */
BlockApp.prototype.on = function(eventName, callback)
{
	this.createBinds(eventName, callback);
}

/**
 * fire() Trigger a custom event in app root's context
 * @param {String} eventName
 * @param {String} value
 * @param {Function} callback
 */
BlockApp.prototype.fire = function(eventName, value, callback)
{
	this.trigger(eventName, {value: value}, callback);
}

/**
 * createBinds() Add an event listener to app root and bind multiple actions
 * to it
 * @param {String} eventName
 * @param {Object Array} eventActions (array of callback functions)
 */
BlockApp.prototype.createBinds = function(eventName, eventActions)
{
	this.binds[eventName] = function(event){

		if (typeof eventActions === "array") {
			for (var callback, i=0; callback = eventActions[i]; i++) {
				if (typeof callback === "function")
					callback(event);
			}
		}
		else if (typeof eventActions === "function") {
			eventActions(event);
		}
	}

	this.root.addEventListener(eventName, this.binds[eventName]);
}

/**
 * removeBinds() Remove an event listener and all bound actions from app
 * root.
 * @param {String} eventName
 */
BlockApp.prototype.removeBinds = function(eventName)
{
	this.root.removeEventListener(eventName, this.binds[eventName], false);
	delete this.binds[eventName];
}

/**
 * trigger() Uses the javascript's custom event API to trigger custom events
 * with additional attributes and a callback
 * @param {String} eventName
 * @param {Object} eventAttributes
 * @param {Function} callback
 */
BlockApp.prototype.trigger = function(eventName, eventAttributes, callback)
{
	try {
	var event = new Event(eventName);
	}
	// Limited IE support
	catch(e) {
	var event = document.createEvent("CustomEvent");
	event.initCustomEvent(eventName, true, true, true);
	}

	if (typeof eventAttributes === "object") {	
		for (var attribute in eventAttributes)
			event[attribute] = eventAttributes[attribute];
	}

	this.root.dispatchEvent(event);

	if (typeof callback === "function")
		callback();
}

/**
 * createTriggers() Bind a DOM node to trigger custom events on a regular event.
 * For example on node "click" trigger N actions
 * @param {Object HTMLElement} node
 * @param {String} bindName
 * @param {Object} bindAttributes
 * @param {Object Array} triggers (list of trigger names)
 */
BlockApp.prototype.createTriggers = function(node, bindName, bindAttributes, triggers)
{
	var _this = this;

	var triggerActions = function(event){

		/* Additional bind attributes can specify preventDefault() or stopPropagation(). They
		are not currently in use however */
		if (typeof bindAttributes === "object" && bindAttributes !== null) {
			
		if (typeof bindAttributes.preventDefault !== "undefined" && bindAttributes.preventDefault == true)
			event.preventDefault();

		if (typeof bindAttributes.stopPropagation !== "undefined" && bindAttributes.stopPropagation == true)
			event.stopPropagation();	
		}

		/* Triggers has to be an array */
		if (Object.prototype.toString.call(triggers) === "[object Array]")
		{
		for (var trigger, i=0; trigger = triggers[i]; i++) {
			
			if (typeof trigger !== "string")
				continue;

			var attributes = {};

			attributes.name = node.bjs.name;
			attributes.index = (typeof node.bjs.index !== "undefined") ? node.bjs.index : null;

			// You can supply the trigger value along with its name in parenthesis
			var n = trigger.indexOf("(")
			var m = trigger.indexOf(")");

			// Parse value from parenthesis
			if (n !== -1 && m !== -1) {
				attributes.value = trigger.slice(n+1, m);
				var trigger = trigger.slice(0, n);
			}

			// For nodes that are block type, get the value through default get() method
			// and pass through filter
			else if (node.bjs.blockjsType == "block") {
				attributes.value = node.bjs.outFilter(node.bjs.get());
			}

			else {
				attributes.value = null;
			}

			// Try to parse into JSON
			try {
				attributes.value = JSON.parse(attributes.value)
			} catch(e) {}

			_this.trigger(trigger, attributes);
		}
		}
	}

	/* Bind trigger custom actions on a node regular event */
	node.bjs.bind(bindName, triggerActions);
}

/**
 * removeTriggers() Remove event listener and bound actions from DOM node
 * @param {Object HTMLElement} node
 * @param {String} bindName
 */
BlockApp.prototype.removeTriggers = function(node, bindName)
{
	node.removeEventListener(bindName, node.bjs.binds[bindName]);
}

/** 
 * deployView() Show a view and all its parent views that belong to the same chain.
 * This allows subviews to be deployed without losing the outer view
 * @param {String} deployViewName
 */
BlockApp.prototype.deployView = function(deployViewName)
{
	if (typeof this.views[deployViewName] === "undefined") {
		// Error: view not found
		this.error("unrecognized view '" + deployViewName + "'");
		return;
	}

	for (var viewName in this.views) {
		var view = this.views[viewName];

		// Withdraw views that are not in deploy view's chain
		if (viewName === deployViewName || this.views[deployViewName].bjs.chain.indexOf(viewName) !== -1) {

			if (view.bjs.active == false)
				view.bjs.deploy();

			for (var triggerName, i=0; triggerName = view.bjs.additionalTriggers.deploy[i]; i++)
				this.fire(triggerName, null);
		}

		// Deploy views that are in deploy view's chain
		else {

			view.bjs.withdraw();

			for (var triggerName, i=0; triggerName = view.bjs.additionalTriggers.withdraw[i]; i++)
				this.fire(triggerName, null);
		}

	}

	// Change location hash
	window.location.hash = this.views[deployViewName].bjs.name;

	// Fire a generic view change event
	this.fire(this.eventNames.genericViewChange, deployViewName);
}

/**
 * configureSelf() Setup app properties from a configuration array. Additionally
 * apply new settings to all views and blocks
 * @param {Object} options
 */
BlockApp.prototype.configureSelf = function(options)
{
	if (typeof options !== "object")
		return;

	for (var attribute in options) {
		if (typeof this[attribute] !== "undefined")
			this.settings[attribute] = options[attribute];
	}

	if (typeof options.updateViews !== "undefined" && options.updateViews == true)
	{
		for (var viewName in this.views) {
			for (var attribute in options) {
				if (typeof this.views[viewName].bjs[attribute] !== "undefined")
					this.views[viewName].bjs[attribute] = options[attribute];
			}
		}
	}

	if (typeof options.updateBlocks !== "undefined" && options.updateBlocks == true)
	{
		for (var blockName in this.blocks) {
			for (var attribute in options) {
				if (typeof this.blocks[blockName].bjs[attribute] !== "undefined")
					this.blocks[blockName].bjs[attribute] = options[attribute];
			}
		}
	}
}

/** configre() Setup target (view or block) properties from a configuration array.
 * @param {String} target
 * @param {Object} options
 */
BlockApp.prototype.configure = function(target, options)
{
	/* Options passed as the first argument */
	if (typeof target === "object")
		return this.configureSelf(target);

	if (typeof this.views[target] !== "undefined") {
		var target = this.views[target];
	}

	else if (typeof this.blocks[target] !== "undefined") {
		var target = this.blocks[target];
	}

	else return;

	for (var attribute in options) {
		if (typeof target.bjs[attribute] !== "undefined")
			target.bjs[attribute] = options[attribute];
	}
}

/**
 * Functions() constructor creates an object that stores some common functions used
 * in BlockApp (to which it contains a circular reference)
 * @param {Object} parent
 */
BlockApp.Functions = function(parent)
{
	// Parent is always app
	this.parent = parent;

	// Simple easingn functions
	this.easingFunctions = {

    linear: function(progress) {
        return progress;
    },
    quadratic: function(progress) {
        return Math.pow(progress, 2);
    },
    swing: function(progress) {
        return 0.5 - Math.cos(progress * Math.PI) / 2;
    },
    circ: function(progress) {
        return 1 - Math.sin(Math.acos(progress));
    },
    back: function(progress, x) {
        return Math.pow(progress, 2) * ((x + 1) * progress - x);
    },
    bounce: function(progress) {
        for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
            if (progress >= (7 - 4 * a) / 11) {
                return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
            }
        }
    },
    elastic: function(progress, x) {
        return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
    }

    } // easingFunctions
}

/**
 * Functions.bind() Add an event listener to a DOM node with callback
 * @param {Object HTMLElement} element
 * @param {String} eventName
 * @param {Function} callback
 */
BlockApp.Functions.prototype.bind = function(element, eventName, callback)
{
	element.bjs.binds[eventName] = callback;

	element.addEventListener(eventName, function(event){
		if (typeof callback == "function")
			callback(event);
	});
}

/** 
 * Functions.show() Make element visible via transition function. Basically restores
 * element's original opacity, position and left attributes
 * @param {Object HTMLElement} element
 */
BlockApp.Functions.prototype.show = function(element)
{

	var _this = this;

	if (element.bjs.transition === "fade"){
		
		this.fadeIn(element, {
			duration: element.bjs.transitionTime,
		});
	}

	// Extend transition functions here

	else {
		element.style.opacity = element.bjs.defaultOpacity;
		element.style.position = element.bjs.defaultPosition;
		element.style.left = element.bjs.defaultLeft;
	}
}

/**
 * Functions.hide() Make element invisible via transition function. Basically sets
 * the element's opacity to 0, position to 'absolute' and left to '-9999px' maintaining
 * its dimensions
 * @param {Object HTMLElement} element
 */
BlockApp.Functions.prototype.hide = function(element)
{

	if (element.bjs.transition === "fade"){
		
		this.fadeOut(element, {
			duration: element.bjs.transitionTime
		});
	}

	// Extend transition functions here

	else {
		element.style.opacity = 0;
		element.style.position = "absolute";
		element.style.left = "-99999px";
	}
}

/**
 * Functions.addClass() Adds a CSS class or multiple classes to a DOM node
 * @param {Object HTMLElement} element
 * @param {String} classNames (space delimited)
 */ 
BlockApp.Functions.prototype.addClass = function(element, classNames)
{

	var addClassesArray = classNames.split(" ");

	for (var className, classIndex=0; className = addClassesArray[classIndex]; classIndex++)
	{
		if (element.classList.contains(className) !== true)
			element.classList.add(className);
	}
}

/**
 * Functions.removeClass() Removes a CSS class or multiple class from a DOM node
 * @param {Object HTMLElement} element
 * @param {String} classNames (space delimited)
 */
BlockApp.Functions.prototype.removeClass = function(element, classNames)
{
	var removeClassesArray = classNames.split(" ");

	for (var className, classIndex=0; className = removeClassesArray[classIndex]; classIndex++)
	{
		if (element.classList.contains(className))
			element.classList.remove(className);
	}
}

/**
 * Functions.setAttributes() Sets element's HTML attribute or multiple attributes 
 * @param {Object HTMLElement} element
 * @param {Object} attributes
 */
BlockApp.Functions.prototype.setAttributes = function(element, attributes)
{
	if (typeof attributes !== "object")
		return;

	for (var attributeName in attributes){
		element.setAttribute(attributeName, attributes[attributeName]);
	}
}

/**
 * Functions.removeAttributes() Removes element's HTML attribute or multiple attributes
 * @param {Object HTMLElement} element
 * @param {Object Array} / {Object} / {String} attributes
 */
BlockApp.Functions.prototype.removeAttributes = function(element, attributes)
{
	if (typeof attributes === "array" || typeof attributes === "object") {
		for (var attributeName, i=0; attributeName = attributes[i]; i++){
			element.removeAttribute(attributeName);
		}
	}

	else if (typeof attributes === "string") {
		element.removeAttribute(attributes);
	}
}

/**
 * Functions.animate() Similar to jQuery's animate function
 * @param {Object} options
 * 
 * options params:
 * @param {Function} options.complete
 * @param {Number} options.duration (in ms)
 */
BlockApp.Functions.prototype.animate = function(options)
{
    
    var _this = this;
    
    var start = new Date;

    var id = setInterval(function() {

        var timePassed = new Date - start;
        var progress = timePassed / options.duration;

        if (progress > 1) {
            progress = 1;
        }

        options.progress = progress;

        var delta = options.delta(progress);
        options.step(delta);

        if (progress == 1) {
            clearInterval(id);

            if (typeof options.complete == "function")
            	options.complete();
        }
    }, options.delay || 10);
}

/**
 * Functions.fadeOut() Fade out transition function animates hide via opacity
 * @param {Object HTMLElement} element
 * @param {Object} options
 */
BlockApp.Functions.prototype.fadeOut = function(element, options)
{
   
    var _this = this;
    var to = 1;

    this.animate({

        duration: options.duration,
        complete: options.complete,

        delta: function(progress) {
            progress = this.progress;
            return _this.easingFunctions.swing(progress);
        },
        
        step: function(delta) {
            element.style.opacity = to - delta;
            if (element.style.opacity == 0) {

            	element.style.position = "absolute";
            	element.style.left = "-9999px";
            }
        }
    });
}

/**
 * Functions.fadeIn() Fade in transition function animates show via opacity
 * @param {Object HTMLElement} element
 * @param {Object} options
 */
BlockApp.Functions.prototype.fadeIn = function(element, options)
{
    var _this = this;
    var to = 0;
    
    this.animate({

        duration: options.duration,
        complete: options.complete,

        delta: function(progress) {
            progress = this.progress;
            return _this.easingFunctions.swing(progress);
        },

        step: function(delta) {
            element.style.opacity = (1-element.bjs.defaultOpacity) + delta;
            if (element.style.opacity >= element.bjs.defaultOpacity) {

            	element.style.position = element.bjs.defaultPosition;
				element.style.left = element.bjs.defaultLeft;
            }
        }
    });
}

/**
 * Data() constructor creates a data object that by default monitors all blocks' change
 * events to keep in sync with their latest values. It can be accessed like this:
 * appObject.data.blockName
 * @param {Object BlockApp} parent
 */ 
BlockApp.Data = function(parent)
{
	this.parent = parent;
	this.binds = {};
	this.update(true);
}

/**
 * Data.update() Get the latest block values. First call with setupBinds=true
 * to keep syncing after call
 * @param {Boolean} setupBinds
 */
BlockApp.Data.prototype.update = function(setupBinds)
{
	var _this = this;

	for (var blockName in this.parent.blocks)
	{
		var block = this.parent.blocks[blockName];

		this[blockName] = block.bjs.get();

		if (typeof setupBinds !== "undefined" &&
			setupBinds == true && 
			typeof _this.binds[block.bjs.name] !== "function") {
			
			var getValueFunction = function(e){
				_this[e.name] = e.value;
			}

			_this.parent.root.addEventListener(block.bjs.defaultTrigger, getValueFunction);

			_this.binds[block.bjs.name] = getValueFunction;
		}
	}
}

/**
 * Data.destroy() Destroy self and remove binds from parent
 */
BlockApp.Data.prototype.destroy = function()
{
	var _this = this;

	for (var bindName in this.binds)
	{
		_this.parent.root.removeEventListener(bindName, this.binds[bindName]);
	}
}
