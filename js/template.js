$ = jQuery;

$(window).load(function(){

    var theWindow        = $(window),
        $bg              = $("#bg"),
        aspectRatio      = $bg.width() / $bg.height();
                                
    function resizeBg() {       
        if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
            $bg
                .removeClass()
                .addClass('bgheight');
        } else {
            $bg
                .removeClass()
                .addClass('bgwidth');
        }           
    }          

    theWindow.resize(resizeBg).trigger("resize");


});

function documentReady(){

	
	setTimeout(function(){
		goranus.deployView("welcomeView");
		window.dispatchEvent(new Event("resize"));
	},
	500);

	var viewWatchdog = setInterval(function(){
		for (var viewName in goranus.views){

			var view = goranus.views[viewName];

			if (view.bjs.active == true && view.style.opacity == "0" && view.bjs.name !== "dummyView") {

				//alert("Watchdog");

				goranus.deployView("dummyView");
				goranus.deployView(view.bjs.name);
				window.dispatchEvent(new Event("resize"));
				
				//alert("Watchdog deployed view: " + view.bjs.name);
			}
		}
	},
	1000);

	$(document).swipe({
		swipe:function(event, direction, distance, duration, fingerCount, fingerData){

			if (direction == "up" || direction == "down") {
				$(".bottom-controls").css("top", 9999);
				$(".bottom-controls").css("opacity", "0");
			}

			if (direction == "left" && distance > 150) {
				history.go(1);
			}

			if (direction == "right" && distance > 150) {
				history.go(-1);
			}
		}

	});

	$(document).swipe("option", "allowPageScroll", $.fn.swipe.pageScroll.VERTICAL);

	previousScrollTop = 0;
	currentScrollTop = 0;
	scrollCheckInterval = null;

	$(".bottom-controls").css("top",  $(window).height() + $(document).scrollTop());
	
	$(document).on("touchmove", function(){
		$(".bottom-controls").css("top", 9999);
		$(".bottom-controls").css("opacity", "0");
	});

	$(document).on("scroll", function(){

		$(".bottom-controls").css("top", 9999);
		$(".bottom-controls").css("opacity", "0");

		if (scrollCheckInterval == null) {
			
			scrollCheckInterval = setInterval(function(){
				
				currentScrollTop = $(document).scrollTop();

				if (currentScrollTop == previousScrollTop) {

					setTimeout(function(){
						var height = $(window).height();
						var scrollTop = $(document).scrollTop();

						$(".bottom-controls").css("opacity", "1");
						$(".bottom-controls").css("top", height + scrollTop + 44);
						$(".bottom-controls").stop().animate({top: height + scrollTop - 44});
					}, 50);

					clearInterval(scrollCheckInterval);
					scrollCheckInterval = null;
				}

				previousScrollTop = currentScrollTop;

			}, 100);

		}
	});//*/
	
	/* Detect if iOS WebApp Engaged and permit navigation without deploying Safari */
	(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(d.href.indexOf("http")||~d.href.indexOf(e.host))&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone")
}

/*
$(document).ready(function(){
	documentReady();
});*/